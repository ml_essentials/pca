import numpy as np
from sklearn.decomposition import PCA


def pca_with_scikit_learn(matrix, k):
    matrix_std = standardize(matrix)
    pca = PCA(n_components=k)
    matrix_pca = pca.fit_transform(matrix_std)
    return matrix_pca


def pca_from_scratch(matrix, k):
    matrix_std = standardize(matrix)
    cov_matrix = calc_covariance_matrix(matrix_std)
    eigenvalues, eigenvectors = eigen_matrix(cov_matrix)
    chosen_eigenvectors = eigenvectors[:, :k]
    matrix_pca = transform(matrix_std, chosen_eigenvectors)
    return matrix_pca


def standardize(matrix):
    mean = np.mean(matrix, axis=0)
    std = np.std(matrix, axis=0)
    return (matrix - mean) / std


def calc_covariance_matrix(matrix) -> np.ndarray:
    _, n = matrix.shape
    cov_matrix = np.ones((n, n))
    for i in range(n):
        for j in range(n):
            cov_matrix[i][j] *= covariance(matrix, i, j)
    return cov_matrix


def covariance(matrix, feature1, feature2):
    matrix1 = matrix[:, feature1]
    matrix2 = matrix[:, feature2]
    return np.sum(matrix1 * matrix2) / matrix.shape[0]


def eigen_matrix(matrix: np.ndarray) -> tuple:
    return np.linalg.eig(matrix)


def transform(matrix, eigenvectors):
    return np.matmul(matrix, eigenvectors)


if __name__ == '__main__':
    matrix = np.array([[1, 2, 3, 4],
                       [5, 5, 6, 7],
                       [1, 4, 2, 3],
                       [5, 3, 2, 1],
                       [8, 1, 2, 2]])

    k = 3
    print('*******')
    result = pca_with_scikit_learn(matrix, k)
    print(result.shape)
    print('-----')
    print(result)
    print('******')

    result = pca_from_scratch(matrix, k)
    print(result.shape)
    print('-----')
    print(result)
    print('******')
